﻿using System;
using System.Linq;

using System.Collections.ObjectModel;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Client;
using System.Configuration;
using Microsoft.Crm.Sdk.Messages;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Query;
using System.Windows;

namespace Challenge_One.ViewModel
{
    class ContactViewModel
    {
        private OrganizationService _orgService;

        public ObservableCollection<Contact> Contacts
        {
            get;
            set;
        }

        public void Connect()
        {
            try
            {
                // Establish a connection to the organization web service using CrmConnection.
                //CrmConnection connection = CrmConnection.Parse("Url=https://cmtwo.mycrmhosted.net;Username=mjwestmore@yahoo.co.uk;Password=&%vV{}sPVU");
                CrmConnection connection = CrmConnection.Parse(
                    "Url=" + ConfigurationManager.AppSettings["url"].ToString() +
                    ";Username=" + ConfigurationManager.AppSettings["username"].ToString() +
                    ";Password=" + ConfigurationManager.AppSettings["password"].ToString());

                // Obtain an organization service proxy.
                // The using statement assures that the service proxy will be properly disposed.
                using (_orgService = new OrganizationService(connection))
                {

                    // Obtain information about the logged on user from the web service.
                    Guid userid = ((WhoAmIResponse)_orgService.Execute(new WhoAmIRequest())).UserId;

                    // Entity systemuser = _orgService.Retrieve("systemuser", userid, new ColumnSet(new string[] { "firstname", "lastname" }));
                    // MessageBox.Show("User Id: " + userid + "\nUser Name: " + systemuser.Attributes["firstname"].ToString() + " " + systemuser.Attributes["lastname"].ToString());

                    SystemUser systemUser = (SystemUser)_orgService.Retrieve("systemuser", userid,
                        new ColumnSet(new string[] { "firstname", "lastname" }));

                    MessageBox.Show("SUCCESSFULLY CONNECTED!\n\nUser Id: " + userid + "\nUser Name: " + systemUser.FirstName + " " + systemUser.LastName);
                }
            }

            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>)
            {
                // You can handle an exception here or pass it back to the calling method.
                throw;
            }
        }

        public void LoadContacts()
        {
 
            ObservableCollection<Contact> contacts = new ObservableCollection<Contact>();

            ServiceContext svcContext = new ServiceContext(_orgService);

            var queryContacts = from c in svcContext.ContactSet
                                select new Contact
                                {
                                    FirstName = c.FirstName,
                                    LastName = c.LastName
                                };

            foreach (var contact in queryContacts)
            {
                contacts.Add(new Contact { FirstName = contact.FirstName, LastName = contact.LastName });
            }

            Contacts = contacts;
        }
    }
}
