﻿using System.Windows;

namespace Challenge_One
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Challenge_One.ViewModel.ContactViewModel contactViewModelObject;

        public MainWindow()
        {
            InitializeComponent();
            retrieveContacts.IsEnabled = false;
            ContactViewControl.Visibility = System.Windows.Visibility.Hidden;
        }

        private void connect_Click(object sender, RoutedEventArgs e)
        {
            contactViewModelObject = new Challenge_One.ViewModel.ContactViewModel();
            contactViewModelObject.Connect();
            retrieveContacts.IsEnabled = true;
            connect.IsEnabled = false;
        }

        private void retrieveContacts_Click(object sender, RoutedEventArgs e)
        {
            contactViewModelObject.LoadContacts();
            ContactViewControl.DataContext = contactViewModelObject;

            ContactViewControl.Visibility = System.Windows.Visibility.Visible;
        }

    }
}
